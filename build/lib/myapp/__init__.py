import myapp.config
from myapp.controllers.movie import api01


# Get the application instance
app = config.app

# register blueprint '/movies'
app.register_blueprint(api01, url_prefix='/movies')

@app.route('/')
# @hello_middleware
def index():
    return 'Hello, World! mongo-flask'


@app.route('/hello', methods=['GET'])
def hello_world_2():
    return 'hello section not hit middleware!'

