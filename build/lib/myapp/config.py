import os
from flask import Flask
from flask_mongoengine import MongoEngine
from flask_cors import CORS
# from myapp.auth_middleware import auth_middleware

app = Flask(__name__)

app.config['MONGODB_SETTINGS'] = {
    'host': os.getenv("MONGO_HOST") 
    #'mongodb://localhost:27017/restdb'
}
# initialize MongoEngine by calling
db = MongoEngine(app)

# enable CORS in Flask
CORS(app)

# disable strict slashes
app.url_map.strict_slashes = False

