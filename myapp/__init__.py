import os
import myapp.config
from myapp.middlewares.auth import auth_decorator
from myapp.controllers.movie import api01
from myapp.controllers.person import api02


# Get the application instance
app = config.app

# register blueprint '/movies'
app.register_blueprint(api01, url_prefix='/movies')
# register blueprint '/persons'
app.register_blueprint(api02, url_prefix='/persons')


# ----------------------
# Setup the Flask-JWT-Extended extension
# app.config['MIDDLEWARE_URL_IDENTITY']     = os.getenv("APP_URL")
# app.config['MIDDLEWARE_VERIFY_ENDPOINT']  = '/token/verify'
# app.config['MIDDLEWARE_BEARER']           = True
# app.config['MIDDLEWARE_VERIFY_HTTP_VERB'] = 'GET'
# app.config['JWT_SECRET']                  = os.getenv("SECRET_KEY")
# app.config['JWT_ALGORITHMS']              = ['HS256']

# middleware = Middleware(app)
# ----------------------

@app.route('/')
def index():
    return 'Hello, World! mongo-flask'


@app.route('/hello', methods=['GET','POST','PUT','DELETE'])
@auth_decorator()
def hello_world_2():
    return 'hello section hited middleware!'

