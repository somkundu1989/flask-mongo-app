from myapp.config import db

class Movie(db.Document):
    name = db.StringField(required=True, unique=True)
    sensored = db.StringField(required=True)
    release = db.StringField(required=True)
    casts = db.ListField(db.StringField(), required=True)
    genres = db.ListField(db.StringField(), required=True)

