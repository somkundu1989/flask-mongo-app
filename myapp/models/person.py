from myapp.config import db

class Person(db.Document):
    name = db.StringField(required=True)
    email = db.StringField(required=True, unique=True)
    dob = db.StringField(required=True)
    place = db.StringField(required=True)
    activities = db.ListField(db.StringField(), required=True)

