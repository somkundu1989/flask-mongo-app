from flask import Blueprint, request, Response
from myapp.models.person import Person


api02 = Blueprint('_api02', __name__)

@api02.route('/')
def get_persons():
    persons = Person.objects().to_json()
    return Response(persons, mimetype="application/json", status=200)

@api02.route('/', methods=['POST'])
def add_person():
    body = request.get_json()
    person =  Person(**body).save().to_json()
    return Response(person, mimetype="application/json", status=200)

@api02.route('/<id>', methods=['PUT'])
def update_person(id):
    body = request.get_json()
    Person.objects.get(id=id).update(**body)
    person = Person.objects.get(id=id).to_json()
    return Response(person, mimetype="application/json", status=200)

@api02.route('/<id>', methods=['DELETE'])
def delete_person(id):
    person = Person.objects.get(id=id).to_json()
    Person.objects.get(id=id).delete()
    return Response('Data deleted', mimetype="application/json", status=200)

@api02.route('/<id>')
def get_person(id):
    person = Person.objects.get(id=id).to_json()
    return Response(person, mimetype="application/json", status=200)


