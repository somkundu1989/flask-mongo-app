from flask import Blueprint, request, Response
from myapp.models.movie import Movie


api01 = Blueprint('_api01', __name__)

@api01.route('/')
def get_movies():
    movies = Movie.objects().to_json()
    return Response(movies, mimetype="application/json", status=200)

@api01.route('/', methods=['POST'])
def add_movie():
    body = request.get_json()
    movie =  Movie(**body).save().to_json()
    return Response(movie, mimetype="application/json", status=200)

@api01.route('/<id>', methods=['PUT'])
def update_movie(id):
    body = request.get_json()
    Movie.objects.get(id=id).update(**body)
    movie = Movie.objects.get(id=id).to_json()
    return Response(movie, mimetype="application/json", status=200)

@api01.route('/<id>', methods=['DELETE'])
def delete_movie(id):
    movie = Movie.objects.get(id=id).to_json()
    Movie.objects.get(id=id).delete()
    return Response('', mimetype="application/json", status=200)

@api01.route('/<id>')
def get_movie(id):
    movie = Movie.objects.get(id=id).to_json()
    return Response(movie, mimetype="application/json", status=200)


