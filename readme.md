

$virtualenv -p python3 venv


$source venv/bin/activate


$pip install -r requirements.txt


$export FLASK_APP=myapp


$flask run --host=0.0.0.0 --port=5050
